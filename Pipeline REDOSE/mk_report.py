#!/usr/bin/env python3

###########
# IMPORTS #
###########

import base64
from flask import Flask, escape, request
from jinja2 import Environment, FileSystemLoader
from weasyprint import HTML
import pandas as pd 
import numpy as np 
import argparse
import pickle
from datetime import datetime
import pdfkit, os

########
# ARGS #
########

parser = argparse.ArgumentParser(description='Script used to generate a pdf report of current analysis')

parser.add_argument('-d', '--workflow_plot', default='workflow_DAG.png', required=True, help='DAG .png file generated from snakemake workflow')
parser.add_argument('-s', '--simil_plot', default='seq_conservation_plot.png', required=True, help='Path to plot file displaying sequence similarity along alignment')
parser.add_argument('-w', '--weblogo_plot', default='weblogo_attB_attP.png', required=True, help='Path to plot file displaying sequence weblogo')
parser.add_argument('-c', '--correspondence_table', default='correspondence_table.csv', required=True, help='Path to .csv file  containing final results of analysis')
parser.add_argument('-p', '--params',required=True, type=str, help="Job parameters pickle file path")
parser.add_argument('-e', '--export_pdf', required=False, action='store_true', help="Export a pdf file of .html report")
parser.add_argument('-j', '--jobdir', type=str, required=False, default="./", help="Directory where workflow output files will be stored")
parser.add_argument('-v', '--verbose', required=False, action="store_true", help='Verbose output')

opt = parser.parse_args()

########
# MAIN #
########

def main():

    # Read csv input file
    df = pd.read_csv(opt.correspondence_table,sep="\t",index_col=None,header=0)
    # Read workflow parameters
    params = pickle.load(open(opt.params,'rb'))
    # Load template
    env = Environment(loader=FileSystemLoader("./templates"))
    template = env.get_template("report.html")

    [
        open(os.path.abspath('css/all.min.css'),'r').read(),
        open(os.path.abspath('css/dataTables.bootstrap4.css'),'r').read(),
        open(os.path.abspath('css/sb-admin.css'),'r').read(),
        open(os.path.abspath('css/additionnal.css'),'r').read()
    ]

    template_vars = {
        'title' : "REDOSE analysis report",
        'today_date' : datetime.now(),
        'table' : df,
        'params' : params,
        'workflow_plot' : base64.b64encode(open(os.path.abspath(opt.workflow_plot),'rb').read()),
        'simil_plot' : base64.b64encode(open(os.path.abspath(opt.simil_plot),'rb').read()),
        'weblogo_plot' : base64.b64encode(open(os.path.abspath(opt.weblogo_plot),'rb').read()),
        'css' :     [
        open(os.path.abspath('css/all.min.css'),'r').read(),
        open(os.path.abspath('css/dataTables.bootstrap4.css'),'r').read(),
        open(os.path.abspath('css/sb-admin.css'),'r').read(),
        open(os.path.abspath('css/additionnal.css'),'r').read()
    ]
    }
    # Render our file and create the PDF using our css style file
    html_out = template.render(template_vars)

    print("Export .html report of analysis...")

    with open(opt.jobdir+'/analysis_report.html','w') as f:

        f.write(html_out)

    options = {
        'page-size': "A4",
        'encoding': "UTF-8"
    }

    if opt.export_pdf == True:

        print("Export .pdf report of analysis...")

        pdfkit.from_string(html_out, options=options, output_path=opt.jobdir+'/analysis_report.pdf')#,css=['css/all.min.css', 'css/dataTables.bootstrap4.css', 'css/sb-admin.css', 'css/additionnal.css'], options=options, output_path=opt.jobdir+'/analysis_report.pdf')
        

if __name__ == '__main__':
    main()