#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import argparse, os, re, multiprocessing, pickle
from snakemake import snakemake
from datetime import datetime
import math

# Get cpu core number

CORE_NB = multiprocessing.cpu_count()

# SCRIPT PARAMETERS

parser = argparse.ArgumentParser(description='Main pipeline script, used to launch the full SnakeMake workflow')

# SCRIPT 1 arguments

parser.add_argument('-n', '--amplicon_file', type=str, required=False, help='Amplicon .fq file path (if not simulated)') # If not simulated
# parser.add_argument('-s', '--simulate_dataset', required=False, action="store_false", help='Simulate input fastq dataset (default: true)')

parser.add_argument('-s1', '--core_genome_size', type=int, required=False, default=14, help='Generated core genome sequences size (if simulated)') # If simulated
parser.add_argument('-s2', '--barcode_size', type=int, required=False, default=10, help='Generated barcode sequences size (if simulated)') # If simulated
parser.add_argument('-s3', '--core_genome_number', type=int, required=False, default=100, help='Generated core genome sequence number (if simulated)') # If simulated

parser.add_argument('-m1', '--cgen_p_mutated', type=float, required=False, default=0.5, help='Percentage of core genome sequences to mutate (if simulated)') # If simulated
parser.add_argument('-m2', '--cgen_max_mut_rate', type=float, required=False, default=0.1, help='Maximum rate of single core genome mutations (if simulated)') # If simulated
parser.add_argument('-m3', '--cgen_mut_over_indel_ratio', type=float, required=False, default=4, help='Mutation rate over indel ratio for a single mutation event (if simulated)') # If simulated

parser.add_argument('-r1', '--read_size', type=int, required=False, default=158, help="Art-illumina read size in pb  (if simulated)") # If simulated
parser.add_argument('-r2', '--read_nb', type=int, required=False, default=1, help="Read number per amplicon (if simulated)") # If simulated

# SCRIPT 4 arguments

parser.add_argument('-th', '--simil_threshold', default=70, required=False, help="Similarity threshold used to identify barcodes and core sequences")
parser.add_argument('-ss', '--spacing_cseq', default=10, required=False, help="Minimal gap between two core sequences (used to split selected positions among corresponding sequences)")
parser.add_argument('-ps', '--simil_plot', default='simil_plot.png', required=False, help='output PNG plot file displaying sequence similarity along alignment')
parser.add_argument('-pw', '--weblogo_plot', default='weblogo_attB_attP.png', required=False, help='output PNG plot file displaying sequence weblogo')

# ALL SCRIPT COMMON

parser.add_argument('-p','--params',type=str, required=False, help="Pickle object containing existing parameters")
parser.add_argument('-e','--email',type=str, required=False, help="Email to send a .pdf report of analysis")
parser.add_argument('-t', '--threads', type=int, default=min(6,CORE_NB), help='Number of threads to launch')
parser.add_argument('-nj','--nb_max_pjobs', type=int, required=False, default=100, help="Maximum allowed number of parallel jobs")
parser.add_argument('-nr', '--nb_max_ram', type=str, required=False, default="30G", help="Maximum allowed ram by task")
parser.add_argument('-w', '--workdir', type=str, required=False, default="./", help="Name of pipeline working directory")
parser.add_argument('-j', '--jobname', type=str, required=False, default="REDOSE_job_{}".format(int(round(datetime.timestamp(datetime.now()),0))), help="Name of job directory where workflow output files will be stored")
parser.add_argument('-g', '--cluster', required=False, action='store_true', help="Launch snakemake workflow on cluster")
parser.add_argument('-l', '--launch', required=False, action='store_true', help="Launch a dryrun of the snakemake workflow (not launched)")
parser.add_argument('-v', '--verbose', required=False, action="store_true", help='Verbose output')

opt = parser.parse_args() # recovery of command line parameters

# AMPLICON_FILE_NAME = re.sub(r'.*\/','',opt.amplicon_file)
# AMPLICON_FILE_NAME = re.sub(,AMPLICON_FILE_NAME)    

params = {}

# Create job folder

print("Generate worflow job folder...")

os.system("mkdir -p "+opt.workdir+"/"+opt.jobname)

if opt.params:

    print("Load existing parameters...")
    
    with open(opt.params,'rb') as f:

        params = pickle.load(f)
    
else:

    print("Make new parameters set...")

    params = {
        'NBJOBS' : opt.nb_max_pjobs,
        'NBTHREADS' : opt.threads,
        'MAXRAM' : opt.nb_max_ram,
        'IDS' : list(range(1,opt.threads)),
        'WORKDIR' : opt.workdir,
        'JOBNAME' : opt.jobname,
        'JOBDIR' : opt.workdir+"/"+opt.jobname,
        'AMPLICON' : 'amplicon_pair',
        'SIM_S1' : opt.core_genome_size,
        'SIM_S2' : opt.barcode_size,
        'SIM_S3' : opt.core_genome_number,
        'SIM_M1' : opt.cgen_p_mutated,
        'SIM_M2' : opt.cgen_max_mut_rate,
        'SIM_M3' : opt.cgen_mut_over_indel_ratio,
        'SIM_R1' : opt.read_size,
        'SIM_R2' : opt.read_nb,
        'RET_TH' : opt.simil_threshold,
        'RET_SS' : opt.spacing_cseq,
        'RET_SPLOT' : opt.simil_plot,
        'RET_WPLOT' : opt.weblogo_plot,
        'START' : datetime.now(),
        'END' : None,
        'DURATION' : None
    }

# Save params in a pickle object

print("Save workflow parameters...")

with open(params['JOBDIR']+"/params.pkl","wb") as f:

    pickle.dump(params, f)

if opt.amplicon_file:

    os.system('cp '+opt.amplicon_file+' '+params['JOBDIR']+"/amplicon_pair.fq")


############
# FUNCTION #
############

def mk_snakefile():

    ids_list = ""

    if opt.cluster == False:

        if not opt.amplicon_file:
            nbprocesses = min(int(params['SIM_S3']*params['SIM_R2']/5),params['NBTHREADS'])
        else:
            nbprocesses = params['NBTHREADS']

        ids_list = ','.join([ str(x) for x in list(range(1,nbprocesses+1)) ])

    else:
        if not opt.amplicon_file:
            nbprocesses = min(int(params['SIM_S3']*params['SIM_R2']/5),params['NBJOBS'])
        else:
            nbprocesses = params['NBJOBS']

        ids_list = ','.join([ str(x) for x in list(range(1,nbprocesses+1)) ])

    arr_list = ids_list.split(',')

    workflow = '''IDS = ['''+ids_list+''']

# 10. Final output rule, correspond to analysis output file

rule all:
    input:
        "'''+str(params['JOBDIR'])+'''/correspondence_table.csv",
        "'''+str(params['JOBDIR'])+'''/weblogo_attB_attP.png",
        "'''+str(params['JOBDIR'])+'''/simil_plot.png"
# 9. Generate weblogo sequence plot

rule mk_weblogo:
    output:
        "'''+str(params['JOBDIR'])+'''/weblogo_attB_attP.png"
    input:
        "'''+str(params['JOBDIR'])+'''/correspondence_table.csv"
    shell:
        "python3 mk_weblogo_plot.py -c {input} -w '''+str(params['RET_WPLOT'])+''' -j '''+str(params['JOBDIR'])+'''"

# 8. Merge attB table split files

rule merge_corresp_table:
    output:
        "'''+str(params['JOBDIR'])+'''/correspondence_table.csv"
    input:
        expand("'''+str(params['JOBDIR'])+'''/correspondence_table_{nbchunkB}.csv", nbchunkB=IDS)
    shell:
        \'\'\'
            echo "attB_core\tattB_barcode1\tattB_barcode2\tattB_occ\tattL_core\tattL_barcode1\tattL_occ\tattR_core\tattR_barcode2\tattR_occ\tattP_rebuilt\n" > {output} ;
            cat '''+str(params['JOBDIR'])+'''/correspondence_table_*.csv >> {output} ;            
            sed -i '/^$/d' {output} ;
        \'\'\'
    
# 7. Reconstitute the AttP core sequences and identify corresponding sequences

rule rebuilt_consensus_core:
    output:
        temp("'''+str(params['JOBDIR'])+'''/correspondence_table_{nbchunkB}.csv")
    input:
        attB="'''+str(params['JOBDIR'])+'''/attB_coreseq_table_{nbchunkB}.txt",
        attL="'''+str(params['JOBDIR'])+'''/attL_coreseq.fa",
        attR="'''+str(params['JOBDIR'])+'''/attR_coreseq.fa",
    shell:
        \'\'\'
            if [ ! -s {input.attB} ]
            then
                touch {output} ;
            else
                python3 mk_corresp_table.py -b {input.attB} -l {input.attL} -r {input.attR} -c {wildcards.nbchunkB} -j '''+str(params['JOBDIR'])+''' ;
            fi
        \'\'\'

# 6. AttB core sequence table into chunks

rule split_attB_table:
    output:
        temp("'''+str(params['JOBDIR'])+'''/attB_coreseq_table_{nbchunkB}.txt")
    input:
        "'''+str(params['JOBDIR'])+'''/attB_coreseq_table.txt"
    shell:
        \'\'\'
            python3 chunk.py -f '''+str(params['JOBDIR'])+'''/attB_coreseq_table.txt -n '''+str(len(arr_list))+''' -d "linebreak" -e ".txt"
        \'\'\'    

# 5. Reconstitute the AttB, AttL, AttR core sequences

rule identify_core:
    output:
        attB=temp("'''+str(params['JOBDIR'])+'''/attB_coreseq_table.txt"),
        attL=temp("'''+str(params['JOBDIR'])+'''/attL_coreseq.fa"),
        attR=temp("'''+str(params['JOBDIR'])+'''/attR_coreseq.fa"),
        conservation_plot="'''+str(params['JOBDIR'])+'''/simil_plot.png",
        attB_align=temp("'''+str(params['JOBDIR'])+'''/attB.fq.fa_align"),
        attR_align=temp("'''+str(params['JOBDIR'])+'''/attL.fq.fa_align"),
        attL_align=temp("'''+str(params['JOBDIR'])+'''/attR.fq.fa_align")
    input:
        attB="'''+str(params['JOBDIR'])+'''/attB.fq.fa",
        attL="'''+str(params['JOBDIR'])+'''/attL.fq.fa",
        attR="'''+str(params['JOBDIR'])+'''/attR.fq.fa"     
    shell:
        "python3 retrieve_core.py -s1 '''+str(params['SIM_S1'])+''' -s2 '''+str(params['SIM_S2'])+''' -b {input.attB} -l {input.attL} -r {input.attR} -p '''+str(params['RET_SPLOT'])+''' -t '''+str(params['RET_TH'])+''' -s '''+str(params['RET_SS'])+''' -j '''+str(params['JOBDIR'])+'''"

# 4. Merge chunks into corresponding sequence type file

rule merge_files:
    output:
        attB="'''+str(params['JOBDIR'])+'''/attB.fq.fa",
        attL="'''+str(params['JOBDIR'])+'''/attL.fq.fa",
        attR="'''+str(params['JOBDIR'])+'''/attR.fq.fa"
    input:
        attB = expand("'''+str(params['JOBDIR'])+'''/attB_{nbchunkA}.fq.fa", nbchunkA=IDS),
        attL = expand("'''+str(params['JOBDIR'])+'''/attL_{nbchunkA}.fq.fa", nbchunkA=IDS),
        attR = expand("'''+str(params['JOBDIR'])+'''/attR_{nbchunkA}.fq.fa", nbchunkA=IDS)
    shell:
        \'\'\'
        cat '''+str(params['JOBDIR'])+'''/attB_*.fq.fa > {output.attB};
        cat '''+str(params['JOBDIR'])+'''/attL_*.fq.fa > {output.attL};
        cat '''+str(params['JOBDIR'])+'''/attR_*.fq.fa > {output.attR};
        \'\'\' 

# 3. Identify sequences corresponding respectively to AttB, AttL and AttR

rule fastq_inspector:
    output:
        attB=temp("'''+str(params['JOBDIR'])+'''/attB_{nbchunkA}.fq.fa"),
        attL=temp("'''+str(params['JOBDIR'])+'''/attL_{nbchunkA}.fq.fa"),
        attR=temp("'''+str(params['JOBDIR'])+'''/attR_{nbchunkA}.fq.fa")
    input:
        "'''+str(params['JOBDIR'])+'''/'''+str(params['AMPLICON'])+'''_{nbchunkA}.fq"
    shell:
        \'\'\'
            if [ ! -s {input} ]
            then
                touch {output.attB} ;
                touch {output.attL} ;
                touch {output.attR} ;
            else
                python3 fastq_split_MULTITHREAD.py -fq {input} -c {wildcards.nbchunkA} -t '''+str(params['NBTHREADS'])+''' -j '''+str(params['JOBDIR'])+''' ;
            fi
        \'\'\'

# 2. Split amplicon fastq file in chunks

rule split_amplicon:
    output:
        temp("'''+str(params['JOBDIR'])+'''/'''+str(params['AMPLICON'])+'''_{nbchunkA}.fq")
    input:
        "'''+str(params['JOBDIR'])+'''/'''+str(params['AMPLICON'])+'''.fq"
    shell:    
        "python3 chunk.py -f {input} -n '''+str(len(arr_list))+'''"
'''

    if not opt.amplicon_file :

        workflow += '''

# 1. Create test illumina dataset

rule create_dataset:
    output:
        "'''+str(params['JOBDIR'])+'''/'''+str(params['AMPLICON'])+'''.fq",
        temp("'''+str(params['JOBDIR'])+'''/attB_cgen.txt"),
        temp("'''+str(params['JOBDIR'])+'''/attR_cgen.txt"),
        temp("'''+str(params['JOBDIR'])+'''/attL_cgen.txt"),
        temp("'''+str(params['JOBDIR'])+'''/bcode.txt"),
        temp("'''+str(params['JOBDIR'])+'''/attB.fa"),
        temp("'''+str(params['JOBDIR'])+'''/attL.fa"),
        temp("'''+str(params['JOBDIR'])+'''/attR.fa"),
        temp("'''+str(params['JOBDIR'])+'''/amplicon_pair_dat.sam"),
        temp("'''+str(params['JOBDIR'])+'''/amplicon_pair_dat1.fq"),
        temp("'''+str(params['JOBDIR'])+'''/amplicon_pair_dat2.fq"),
        temp("'''+str(params['JOBDIR'])+'''/amp_reference.fa")
    input:
        "'''+str(params['JOBDIR'])+'''/"
    shell:
        \'\'\'
        python3 dataset_creator.py -n '''+str(params['AMPLICON'])+''' -s1 '''+str(params['SIM_S1'])+''' -s2 '''+str(params['SIM_S2'])+''' -s3 '''+str(params['SIM_S3'])+''' -m1 '''+str(params['SIM_M1'])+''' -m2 '''+str(params['SIM_M2'])+''' -m3 '''+str(params['SIM_M3'])+''' -r1 '''+str(params['SIM_R1'])+''' -r2 '''+str(params['SIM_R2'])+''' -j '''+str(params['JOBDIR'])+'''
        \'\'\'
'''

    workflow = re.sub(r'\/\/',"/",workflow)

    return workflow

########
# MAIN #
########

def main():    

    if opt.amplicon_file:

        os.system("cp "+opt.amplicon_file+" "+params['JOBDIR']+"/"+params['AMPLICON']+".fq")

    snakefile = open(params['JOBDIR']+"/snakefile.wf",'w')

    print("Generate snakemake worflow file...")

    snakefile.write(mk_snakefile())

    snakefile.close()

    if opt.cluster == True:

        slurm_sub = "sbatch -J "+str(opt.jobname)+" -o "+params['JOBDIR']+"/REDOSE_workflow.output"+" -e "+params['JOBDIR']+"/REDOSE_workflow.error"+" --mem "+str(params['MAXRAM'])+" --nodes=1 --ntasks=1 --cpus-per-task="+str(max(CORE_NB,params['NBTHREADS']))

        if opt.launch == True:

            print("Launch REDOSE snakemake workflow (send to SLURM cluster)...")

            snakemake(
                snakefile = params['JOBDIR']+"/snakefile.wf",
                nodes = params['NBJOBS'],
                local_cores = params['NBTHREADS'],
                lock = False,
                cluster = slurm_sub,
                printshellcmds = True,
                verbose = True,
                printdag = False,
                dryrun = True,
                report=params['JOBDIR']+"/job_report.html",
                # ignore_ambiguity = True,
                snakemakepath = opt.workdir+"/bin/snakemake"
            )
        
        else: 

            print("Launch REDOSE snakemake workflow (send to SLURM cluster)...")

            snakemake(
                snakefile = params['JOBDIR']+"/snakefile.wf",
                nodes = params['NBJOBS'],
                local_cores = params['NBTHREADS'],
                lock = False,
                cluster = slurm_sub,
                printshellcmds = True,
                verbose = True,
                printdag = False,
                dryrun = False,
                # ignore_ambiguity = True,
                snakemakepath = opt.workdir+"/bin/snakemake"
            )

    else:
        if opt.launch == True:

            print("Launch REDOSE snakemake workflow (local) ...")

            snakemake(
                snakefile = params['JOBDIR']+"/snakefile.wf",
                cores = params['NBTHREADS'],
                lock = False,
                printshellcmds = True,
                verbose = True,
                printdag = False,
                dryrun = False,
                report=params['JOBDIR']+"/job_report.html",
                # ignore_ambiguity = True,
                snakemakepath = opt.workdir+"/bin/snakemake"
            )
        
        else: 

            print("Launch REDOSE snakemake workflow (local)...")

            snakemake(
                snakefile = params['JOBDIR']+"/snakefile.wf",
                cores = params['NBTHREADS'],
                lock = False,
                printshellcmds = True,
                verbose = True,
                printdag = False,
                dryrun = False,
                # ignore_ambiguity = True,
                snakemakepath = opt.workdir+"/bin/snakemake"
            )
    
    # Computing running time

    params['END'] = datetime.now()
    duration = params['END'] - params['START']

    deltatime = {
        "days" : duration.days,
        "hours" : duration.seconds//60//60,
        "min" : duration.seconds//60 - (duration.seconds//60//60)*3600,
        "sec" : int(round(duration.seconds % 60, 0)),
        "ms" : duration.microseconds
    }

    params['DURATION'] = deltatime

    # Save params in a pickle object

    print("Save workflow parameters...")

    with open(params['JOBDIR']+"/params.pkl","wb") as f:

        pickle.dump(params, f)

    print("Job ran successfully ! Computing time : "+str(params['DURATION']['days'])+"d:"+str(params['DURATION']['hours'])+"h:"+str(params['DURATION']['min'])+"m:"+str(params['DURATION']['sec'])+"s")

    # Export DAG of workflow

    print("Export a DAG representation of workflow...")

    # full workflow dag

    os.system("snakemake --quiet --forceall --dag -s "+params['JOBDIR']+"/snakefile.wf > "+params['JOBDIR']+"/workflow_DAG_full.dot")

    nbprocesses = None

    if opt.cluster == False:
        nbprocesses = min(int(params['SIM_S3']*params['SIM_R2']/5),params['NBTHREADS'])
    else:
        nbprocesses = min(int(params['SIM_S3']*params['SIM_R2']/5),params['NBJOBS'])

    if nbprocesses > 7 :

        # sumarized dag of the workflow

        os.system(
            '''
    unset FILEBODYLABELS;
    unset FILEBODYEDGES;
    unset TMPFILEBODYLABELS;
    unset TMPFILEBODYEDGES;
    unset FILEHEADER;
    unset FILEFOOTER;

    FILEHEADER=`head -n3 '''+params['JOBDIR']+'''/workflow_DAG_full.dot`
    FILEFOOTER=`tail -n1 '''+params['JOBDIR']+'''/workflow_DAG_full.dot`

    FILEBODYLABELS=""
    FILEBODYEDGES=""
    TMPFILEBODYLABELS=""
    TMPFILEBODYEDGES=""

    RULES=`grep "label = " '''+params['JOBDIR']+'''/workflow_DAG_full.dot| awk 'match($0, /([0-9]+)\[label = \"(\w+).+(\w*):?\s?([0-9]*)\",.+\]/, b){print b[2]}'|uniq`

    for rule in $RULES; do

        unset TMPFILEBODYLABELS;
        unset TMPFILEBODYEDGES;
        TMPFILEBODYLABELS="";
        TMPFILEBODYEDGES="";

        if [[ `grep -c "$rule" '''+params['JOBDIR']+'''/workflow_DAG_full.dot` > 1 ]]
        then

            if [[ `grep "$rule" '''+params['JOBDIR']+'''/workflow_DAG_full.dot|grep ':' -c` > 0 ]]
            then
                RULELABELSTOP=`grep "$rule" '''+params['JOBDIR']+'''/workflow_DAG_full.dot | head -n 3`
                RULELABELSCENTER=`grep "$rule" '''+params['JOBDIR']+'''/workflow_DAG_full.dot | head -n 4 | tail -n1| awk '{c=gsub(/\: [0-9]+/,": ...");print}'|cat`
                RULELABELSDOWN=`grep "$rule" '''+params['JOBDIR']+'''/workflow_DAG_full.dot | tail -n 3`

                TMPFILEBODYLABELS=`echo "$RULELABELSTOP#$RULELABELSCENTER#$RULELABELSDOWN"|tr "#" "\n"`
                
                TMPFILEBODYEDGES=`echo "$TMPFILEBODYLABELS"|awk 'match($0, /([0-9]+)\[/, d){print "\t"d[1]" ->"}'|sort|uniq|grep -f - '''+params['JOBDIR']+'''/workflow_DAG_full.dot`
            else
                RULELABELSTOP=`grep "$rule" '''+params['JOBDIR']+'''/workflow_DAG_full.dot | head -n 3`
                RULELABELSCENTER=`grep "$rule" '''+params['JOBDIR']+'''/workflow_DAG_full.dot | head -n 4 | tail -n1`
                RULELABELSDOWN=`grep "$rule" '''+params['JOBDIR']+'''/workflow_DAG_full.dot | tail -n 3`

                TMPFILEBODYLABELS=`echo "$RULELABELSTOP#$RULELABELSCENTER#$RULELABELSDOWN"|tr "#" "\n"`

                TMPFILEBODYEDGES=`echo "$TMPFILEBODYLABELS"|awk 'match($0, /([0-9]+)\[/, d){print "\t"d[1]" ->"}'|sort|uniq|grep -f - '''+params['JOBDIR']+'''/workflow_DAG_full.dot`
            fi

        else
            TMPFILEBODYLABELS=`grep "$rule" '''+params['JOBDIR']+'''/workflow_DAG_full.dot` ;
            TMPFILEBODYEDGES=`grep "$rule" '''+params['JOBDIR']+'''/workflow_DAG_full.dot | awk 'match($0, /([0-9]+)\[label = \"(\w+)\",.+\]/, a){print "\t"a[1]" ->"}'|sort|uniq|grep -f - '''+params['JOBDIR']+'''/workflow_DAG_full.dot` 
        fi

        FILEBODYLABELS=`echo "$FILEBODYLABELS#$TMPFILEBODYLABELS"|tr "#" "\n"` ;
        FILEBODYEDGES=`echo "$FILEBODYEDGES#$TMPFILEBODYEDGES"|tr "#" "\n"` ;

    done;

    SELIDS=`echo "$FILEBODYLABELS"|awk 'match($0, /([0-9]+)\[/, d){print d[1]}'|uniq` ;

    FILEFILTEREDEDGES=`for check in $SELIDS; do echo "$FILEBODYEDGES"|grep -P "\->\ $check$"; done|sort|uniq` ;

    OUTPUT=`echo "$FILEHEADER#$FILEBODYLABELS#$FILEFILTEREDEDGES#$FILEFOOTER"|tr "#" "\n"` ;

    echo "$OUTPUT" > '''+params['JOBDIR']+'''/workflow_DAG_sumarized.dot ;

    echo "$OUTPUT" | dot -Tpng > '''+params['JOBDIR']+'''/workflow_DAG.png ;

    '''
        )
    
    else:

        os.system("cat "+params['JOBDIR']+"/workflow_DAG_full.dot | dot -Tpng >"+params['JOBDIR']+"/workflow_DAG.png")

    # print workflow report

    print("Print job report...")

    os.system("snakemake --quiet --report "+params['JOBDIR']+"/job_report.html"+" -s "+params['JOBDIR']+"/snakefile.wf")

    # Clean temp files

    print("Delete tempory files...")

    os.system("snakemake --quiet --delete-temp-output -s "+params['JOBDIR']+"/snakefile.wf")

    # make analysis report

    os.system('''python3 mk_report.py -d '''+str(params['JOBDIR'])+'''/workflow_DAG.png -s '''+str(params['JOBDIR'])+'''/simil_plot.png -w '''+str(params['JOBDIR'])+'''/weblogo_attB_attP.png -c '''+str(params['JOBDIR'])+'''/correspondence_table.csv -p '''+str(params['JOBDIR'])+'''/params.pkl -j '''+str(params['JOBDIR']))

    # Send a mail with results (if a mail is provided)

    if opt.email:

        print("Zip email content...")

        os.system('''zip '''+str(params['JOBDIR'])+'''/'''+str(params['JOBNAME'])+'''.zip '''+str(params['JOBDIR'])+'''/correspondence_table.csv '''+str(params['JOBDIR'])+'''/simil_plot.png  '''+str(params['JOBDIR'])+'''/weblogo_attB_attP.png '''+str(params['JOBDIR'])+'''/analysis_report.html''')

        print("Send email with job reports...")

        mail_cmd = '''     
MAXSIZE=5000000
ACTUALSIZE=`wc -c '''+str(params['JOBDIR'])+'''/'''+str(params['JOBNAME'])+'''.zip|awk '{print $1}'`

if [ $ACTUALSIZE -ge $MAXSIZE ]; then
    zip '''+str(params['JOBDIR'])+'''/'''+str(params['JOBNAME'])+'''_min.zip '''+str(params['JOBDIR'])+'''/simil_plot.png '''+str(params['JOBDIR'])+'''/weblogo_attB_attP.png '''+str(params['JOBDIR'])+'''/workflow_DAG.png
    echo 'Job success (output files too big, manual download required)' | mail -s '''+str(params['JOBNAME'])+''' -a '''+str(params['JOBDIR'])+'''/'''+str(params['JOBNAME'])+'''_min.zip '''+str(opt.email)+'''
else
    echo 'Job success' | mail -s '''+str(params['JOBNAME'])+''' -a '''+str(params['JOBDIR'])+'''/'''+str(params['JOBNAME'])+'''.zip '''+str(opt.email)+'''
fi   
        '''

        print(mail_cmd)

        os.system(mail_cmd)


if __name__ == '__main__':
    main()
