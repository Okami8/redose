#!/usr/bin/env python3

###########
# IMPORTS #
###########

import argparse, re, os, seqplot
import collections
from collections import Counter
import numpy as np 
import matplotlib.pyplot as plt
from matplotlib.text import TextPath
from matplotlib.patches import PathPatch
from matplotlib.font_manager import FontProperties
import itertools

########
# ARGS #
########

parser = argparse.ArgumentParser(description='Used to generate attB and attP sequence weblogo plot')

parser.add_argument('-c', '--correspondence_table', default='correspondence_table.csv', required=True, help='Path to .csv file  containing final results of analysis')
parser.add_argument('-w', '--weblogo_plot', default='weblogo_attB_attP.png', required=False, help='output PNG plot file displaying sequence weblogo')
parser.add_argument('-d', '--display_plot', required=False, action="store_true", help="Display plots (T/F)")
parser.add_argument('-j', '--jobdir', type=str, required=False, default="./", help="Directory where workflow output files will be stored")
parser.add_argument('-v', '--verbose', required=False, action="store_true", help='Verbose output')

opt = parser.parse_args()

############
# FUNCTION #
############


def flatten(iterable, ltypes=collections.Iterable):
    remainder = iter(iterable)
    while True:
        first = next(remainder)
        if isinstance(first, ltypes) and not isinstance(first, (str, bytes)):
            remainder = itertools.chain(first, remainder)
        else:
            yield first

########
# MAIN #
########

def main():
    # Sequence logo
    # Compute nucleotides frequences:

    attB_all_sequences = []
    attP_all_sequences = []

    print("\n")
    print("Loading correspondance table...")
    print("\n")

    with open(opt.correspondence_table,'r') as f:

        f.readline() # remove the header

        all_rows = [ [ re.sub('\n','',l).split("\t")[i] for i in [0,10] ] for l in f.readlines() ]

        max_length = np.amax([len(row[0]) for row in all_rows])

        for row in all_rows:

            if opt.verbose:

                print("Line: ",row)

            while len(row[0]) < max_length:

                row[0]+="-"
                row[1]+="-"

            # attP = list(flatten([s.split("}") for s in row[10].split("{")]))
  
            # attP_all_sequences.append(list(flatten([ list(ss) if not bool(re.search(",",ss)) == True else "{"+ss+"}" for ss in attP  ])))

            attB_all_sequences.append(row[0])

            attP_all_sequences.append(row[1])

    print("\n")
    print("Compute base frequencies from each attB core sequence position...")
    print("\n")

    # print(attB_all_sequences[0]," ",len(attB_all_sequences[0]))

    ATTB_ALL_SCORES = [[(k, c[k] / len(attB_all_sequences)) for k in c] for c in [dict(c) for c in [ Counter([list(s)[i] for s in attB_all_sequences]) for i in range(len(attB_all_sequences[0])) ] ] ]    
    attP_all_sequences = [re.sub(r'\{(.),.\}',r'\1',s) for s in attP_all_sequences]
    ATTP_ALL_SCORES = [[(k, c[k] / len(attB_all_sequences)) for k in c] for c in [dict(c) for c in [ Counter([list(s)[i] for s in attP_all_sequences]) for i in range(len(attP_all_sequences[0])) ] ] ]

    cscores = {
        'attB' : ATTB_ALL_SCORES,
        'attP' : ATTP_ALL_SCORES
    }

    if opt.verbose == True:

        print("\n")
        print("attB scores : ")
        print(cscores['attB'])
        print("\n")
        print("attP scores : ")
        print(cscores['attP'])

    print("\n")
    print("Compute cgen attB/attP sequences weblogo...")
    print("\n")

    nbsc = len(list(cscores.keys()))
    fig_size = plt.rcParams["figure.figsize"]  
    fig_size[0] = 15  
    fig_size[1] = 8  
    plt.rcParams["figure.figsize"] = fig_size  
    plt.figure(1, dpi=200, figsize=(20,20))    
    fig, axes = plt.subplots(nbsc,1)

    for i in range(nbsc):
        # attB and attP
        k = list(cscores.keys())[i]
        all_scores = cscores[k]        
        x = 1
        maxi = 0

        for scores in all_scores:
            y = 0
            for base, score in scores:
                #if base != "-":
                seqplot.letterAt(base, x,y, score, axes[i])
                y += score
            x += 1
            maxi = max(maxi, y)

        axes[i].set_xlim((0, x))
        axes[i].set_ylim((0, maxi))
        axes[i].set_title(k+" core sequence")
        axes[i].set_xlabel('Position (nt)')
        axes[i].set_ylabel('Frequency (%)')

    fig.tight_layout()

    if opt.display_plot==True:
        plt.show()  

    fig.savefig(opt.jobdir+"/"+opt.weblogo_plot)

    # VERBOSE OPTION : OPTIONAL DISPLAY :

    if opt.verbose:
        print(opt) # command line parameters

    # Removing the useless files

    # removing_useless_files(opt.jobdir+"/"+"amplicon_pair_*.fq")

    # removing_useless_files(opt.jobdir+"/"+"attB_*.fq.fa")
    # removing_useless_files(opt.jobdir+"/"+"attL_*.fq.fa")
    # removing_useless_files(opt.jobdir+"/"+"attR_*.fq.fa")

    # removing_useless_files(opt.jobdir+"/"+"attB.fq.fa_align")
    # removing_useless_files(opt.jobdir+"/"+"attL.fq.fa_align")
    # removing_useless_files(opt.jobdir+"/"+"attR.fq.fa_align")

    # removing_useless_files(opt.jobdir+"/"+"attB_coreseq_table.txt")
    # removing_useless_files(opt.jobdir+"/"+"attL_coreseq_table.txt")
    # removing_useless_files(opt.jobdir+"/"+"attR_coreseq_table.txt")

if __name__ == '__main__':

    main()
