#!/usr/bin/python3
"""
Gracefully forked by a Feral developer thanks to stackoverflow.
We are using tuples because if we have immutable data, it is more efficient.
"""

###########
# IMPORTS #
###########

import argparse, datetime, numpy as np, os.path

# Threading
import queue, sys, threading, multiprocessing

# Biopython
from Bio import pairwise2
from Bio.pairwise2 import format_alignment
from Bio.Seq import Seq

# JSON
import json

# Get cpu core number
CORE_NB = multiprocessing.cpu_count()

###########
# DEFINES #
###########

# These are the alignment possibilities models as global variables

attB_model = {
	"1" : Seq("GTCGACTCTAGAGGATCCCACAAGCTTGACTCGAAATCCGCCGAACCAATGTTNNNNNNNNGCAGGTTCAAATNNNNNNNNNNAATAAGGTCAAATNNNNNNNNTAGGAAGTATTTCAAAGTAGCCAGTAGTTTTAAGAATTCGTGGGATCCCCGGGTAC"),
	"2" : Seq("GTCGACTCTAGAGGATCCCACAAGCTTGACTCGAAATCCGCCGAACCAATGTTNNNNNNNNGCAGGTTCAAATNNNNNNNNNNAATAAGGTCAAATNNNNNNNNTAGGAAGTATTTCAAAGTAGCCAGTAGTTTTAAGAATTCGTGGGATCCCCGGGTAC").reverse_complement()
}

attL_model = {
	"1" : Seq("GTCGACTCTAGAGGATCCCACAAGCTTGACTCGAAATCCGCCGAACCAATGTTNNNNNNNNGCAGGTTCAAATNNNNNNNNNNAATCAAAGCAATAATCCCCGAGAAATCAACATTCTCGGGGATTATTTTTGTTTTTAACTAGAAAATAACTAGAAAGAGCTAG"),
	"2" : Seq("GTCGACTCTAGAGGATCCCACAAGCTTGACTCGAAATCCGCCGAACCAATGTTNNNNNNNNGCAGGTTCAAATNNNNNNNNNNAATCAAAGCAATAATCCCCGAGAAATCAACATTCTCGGGGATTATTTTTGTTTTTAACTAGAAAATAACTAGAAAGAGCTAG").reverse_complement()
}

attR_model = {
	"1" : Seq("AACAACCAGAAACGCTTTTTAGCCCAAAAGCAAGATTCGAAACCTCCAGAACCCTCCAGAAACATGGTTGAAAGAANNNNNNNNNNNAATAAGGTCAAATNNNNNNNNTAGGAAGTATTTCAAAGTAGCCAGTAGTTTTAAGAATTCGTGGGATCCCCGGGTAC"),
	"2" : Seq("AACAACCAGAAACGCTTTTTAGCCCAAAAGCAAGATTCGAAACCTCCAGAACCCTCCAGAAACATGGTTGAAAGAANNNNNNNNNNNAATAAGGTCAAATNNNNNNNNTAGGAAGTATTTCAAAGTAGCCAGTAGTTTTAAGAATTCGTGGGATCCCCGGGTAC").reverse_complement()
}

# Global scope queues

inSequenceQueue = None
outAlignementQueue = None

if __name__ == "__main__":
	# Handle the arguments
	parser = argparse.ArgumentParser(description='Main')
	parser.add_argument('-fq', '--fq_file', required=True, help='Generated core genome sequences file')
	parser.add_argument('-t', '--threads', type=int, default=CORE_NB, help='Number of threads to launch')
	parser.add_argument('-l','--length', type=int, default=158, help="Read minimum length")
	parser.add_argument('-c','--chunk_id',type=str,default="",required=False,help="Id of file chunk")
	parser.add_argument('-j', '--jobdir', type=str, required=False, default="./", help="Directory where workflow output files will be stored")

	opt = parser.parse_args()

	# Check if defined core number is lower of equal than available cores
	if opt.threads > CORE_NB:
		opt.threads = CORE_NB

	# Output files paths

	outputFileAttB = opt.jobdir+"/"+'attB.fq.fa'
	outputFileAttL = opt.jobdir+"/"+'attL.fq.fa'
	outputFileAttR = opt.jobdir+"/"+'attR.fq.fa'

	#########
	# LOGIC #
	#########

	def split_fastQ(in_queue, out_queue):
		"""
		This is the loop running in parallel on multiple threads
		to treat incomming data from main thread which is reading the file.

		We compute the scores of the sequences against the 3 models,
		and output the highest one.
		"""
		score = () # Local tuple first and only declaration (just in case)
		while True:
			# Get some data from the queue
			(sequenceName, sequence) = in_queue.get()
			# print("sequenceName : "+sequenceName)

			strand = sequenceName.split('-')[1].split('/')[-1].replace("\n",'')

			# Process the data, i.e. perform alignments
			# Score is put inside a tuple (again more efficient than list)
			# And report the highest one

			alignments = (
				pairwise2.align.localxs(attB_model[strand], sequence,-10, -2),
				pairwise2.align.localxs(attL_model[strand], sequence,-10, -2),
				pairwise2.align.localxs(attR_model[strand], sequence,-10, -2)
			)

			score = ( alignments[0][0][2], alignments[1][0][2], alignments[2][0][2] )

			# print("sequenceName : "+sequenceName+" "+str(score)+" "+str(np.argmax(score)))

			if(len(np.unique(score))==len(score)):
				
				# Result is a tuple of the form (<OUTPUT FILE PATH NUMBER>, <SEQ NAME + SEQ>)
				# To decrease memopry usage we could only return the OUTPUT FILE
				# and the sequence index or name so it becomes a sorting problem
				out_queue.put( (np.argmax(score), "".join((sequenceName, sequence))) )

				# Notify that sequence was processed

			in_queue.task_done()


	def handle_results(full = False):
		"""
		Processing the results is a separate function as the output queue can reach max capacity.
		This needs to be called if we have no more memory or if queue is full.
		"""

		if full:
			print("Dumping the results...")

		# Open the output files (need to append as it is likely to be opened multiple times)
		
		attB_fq = open(outputFileAttB, "a")
		attL_fq = open(outputFileAttL, "a")
		attR_fq = open(outputFileAttR, "a")

		outputFileDescriptors = (
			attB_fq,
			attL_fq,
			attR_fq
		)

		# Compile the results in the form (<OUTPUT FILE PATH NUMBER>, <SEQ NAME + SEQ>)
		# OUTPUT FILE PATH NUMBER gives the file descriptor in which to write

		while not outAlignementQueue.empty():
			resultTuple = outAlignementQueue.get()
			outputFileDescriptors[resultTuple[0]].write( resultTuple[1] )

		# Close the output files

		attB_fq.close()
		attL_fq.close()
		attR_fq.close()

	# This is how we launch the program
	
	# Verify the output files doesn't already exist
	# if  os.path.isfile(outputFileAttB) or os.path.isfile(outputFileAttL) or os.path.isfile(outputFileAttR):
	# 	print("Output file paths already existed, setting new names.")
	# 	# Changing output file paths
	# 	timeString = datetime.datetime.now().strftime('%d-%b-%Y_%H:%M:%S')
	# 	outputFileAttB = "attB_{}.fq.fa".format(timeString)
	# 	outputFileAttL = "attL_{}.fq.fa".format(timeString)
	# 	outputFileAttR = "attR_{}.fq.fa".format(timeString)

	#Verify the output files doesn't already exist
	if  opt.chunk_id != "":
		print("Output file paths already existed, setting new names.")
		# Changing output file paths
		outputFileAttB = opt.jobdir+"/"+"attB_{}.fq.fa".format(opt.chunk_id)
		outputFileAttL = opt.jobdir+"/"+"attL_{}.fq.fa".format(opt.chunk_id)
		outputFileAttR = opt.jobdir+"/"+"attR_{}.fq.fa".format(opt.chunk_id)

	print("Running {1} threads on sequence file '{0}'".format(opt.fq_file, str(opt.threads)))

	# Initialise Queues for threads
	inSequenceQueue = queue.Queue()
	outAlignementQueue = queue.Queue()

	print("Starting the workers...")

	# Start the workers
	for i in range(opt.threads):
		t = threading.Thread(
			target = split_fastQ,
			args = (inSequenceQueue, outAlignementQueue)
		)
		t.daemon = True
		t.start() 

	# Read the sequences data
	with open(opt.fq_file, "r") as fastq_in:
		# Using the iterable way which provides a buffer
		for sequenceName in fastq_in:

			### Output queue might be full, empty it before resuming ###
			if outAlignementQueue.full():
				print("Output queue is full... :/")
				handle_results(True)

			# Reading the sequence as the next line with the iterable way
			sequence = next(fastq_in)

			if len(sequence)>=opt.length:
				# Feed the workers (tuple is more efficient than list)
				inSequenceQueue.put((sequenceName, sequence))

			# Get rid of the quality lines
			next(fastq_in)
			next(fastq_in)

	print("All sequences read, waiting for workers...")

	# We are mostly done at this point, tell them to stop
	inSequenceQueue.join()

	# Enhancement would be open 3 more threads with each writting the appropriate sequences into its own file

	print("Compiling last results...")

	handle_results()

	os.system(" sed -i 's/@/>/g' "+outputFileAttB)
	os.system(" sed -i 's/@/>/g' "+outputFileAttL)
	os.system(" sed -i 's/@/>/g' "+outputFileAttR)

	print("Process finished. Results are in {0}, {1} and {2}.".format(outputFileAttB, outputFileAttR, outputFileAttL))