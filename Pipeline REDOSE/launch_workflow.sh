#!/usr/bin/env bash 
# REDOSE workflow main script
# Used to activate the python virtual environnement and launch the workflow

# ## Clear out nested functions on exit

trap `unset -f _usage` EXIT RETURN

### Variable declaration with default values

amplicon_file=""
core_genome_number="100"
core_genome_size="14"
barcode_size="10"
core_genome="100"
cgen_p_mutated="0.5"
cgen_max_mut_rate="0.1"
cgen_mut_over_indel_ratio="4"
read_size="158"
read_nb="1"
simil_threshold="70"
spacing_cseq="10"
threads="6"
nb_max_pjobs="100"
nb_max_ram="30G"
workdir="./"
cluster=""
launch=""
verbose=""
params=""
email=""

function _usage(){
    # USAGE function used to display arguments description and help
    cat << EOF
        foobar $Options
    $* 
	Description : Launcher script used to activate workflow environnement, retrieve user parameters and launch workflow
        Usage : 
        Options:
            -a     --amplicon_file                 Amplicon .fq file path (if not simulated)  [default: $amplicon_file]                                      
            -c     --core_genome_size              Generated core sequences size  [default: $core_genome_size]
            -d     --barcode_size                  Generated barcode sequences size (if simulated)  [default: $barcode_size]
            -e     --core_genome_number            Generated core sequences number (if simulated)  [default: $core_genome_number]
            -f     --cgen_p_mutated                Percentage of core sequences to mutate (if simulated)  [default: $cgen_p_mutated]
            -g     --cgen_max_mut_rate             Maximum rate of single core genome mutations (if simulated)  [default: $cgen_max_mut_rate]
            -h     --cgen_mut_over_indel_ratio     Mutation rate over indel ratio for a single mutation event (if simulated)  [default: $cgen_mut_over_indel_ratio]
            -i     --read_size                     Sequencing reads size  [default: $read_size]
            -j     --read_nb                       Read number per amplicon (if simulated)  [default: $read_nb]
            -k     --simil_threshold               Similarity threshold used to identify barcodes and core sequences  [default: $simil_threshold]
            -m     --spacing_cseq                  Minimal gap between two core sequences (used to split selected positions amang corresponging sequences)  [default: $spacing_cseq]
            -n     --threads                       Number of threads to launch  [default: $threads]
            -o     --nb_max_pjobs                  Maximum allowed number of parallel jobs  [default: $nb_max_pjobs]
            -p     --nb_max_ram                    Maximum of RAM allowed by task  [default: $nb_max_ram]
            -r     --workdir                       Path to pipeline working directory  [default: $workdir]
            -q     --cluster                       Launch workflow an SLURM cluster  [default: $cluster]
            -z     --params                        Load existing parameters (defined in previous analysis) [default: $params]
            -y     --email                         Email to send a .pdf report of analysis [default: $email] (TO DO, for a putative V2)
            -l     --launch                        Not launch workflow (dryrun to test worflow or generate DAG scheme)  [default: $launch]
            -v     --verbose                       Verbose output  [default: $verbose]
EOF
}

# Gets the command name without path
cmd(){ echo `basename $0`; }

declare -a ARGS=(-h -v -z -y -q -l -v -a -c -d -e -f -g -i -j -k -m -n -o -p -r --help --verbose --params --email --cluster --launch --verbose --amplicon_file --core_genome_size --barcode_size --core_genome_number --cgen_p_mutated --cgen_max_mut_rate --cgen_mut_over_indel_ratio --read_size --read_nb --simil_threshold --spacing_cseq --threads --nb_max_pjobs --nb_max_ram --workdir)

OPTS=$( getopt -o hqlvy:a:c:d:e:f:g:i:j:k:m:n:o:p:r: -l help,cluster,launch,verbose,email:,amplicon_file:,simulate_dataset:,core_genome_size:,barcode_size:,core_genome_number:,cgen_p_mutated:,cgen_max_mut_rate:,cgen_mut_over_indel_ratio:,read_size:,read_nb:,simil_threshold:,spacing_cseq:,threads:,nb_max_pjobs:,nb_max_ram:,workdir: -- "$@" )
#shortopts = hbqlva:c:d:e:f:g:i:j:k:m:n:o:p:r:
#longopts ="help,simulate_dataset,cluster,launch,verbose,amplicon_file:,simulate_dataset:,core_genome_size:,barcode_size:,core_genome_number:,cgen_p_mutated:,cgen_max_mut_rate:,cgen_mut_over_indel_ratio:,read_size:,read_nb:,simil_threshold:,spacing_cseq:,threads:,nb_max_pjobs:,nb_max_ram:"

# if [ $# = 0 ]
# then
#     _usage " >>> ERROR : no options given ! ";
#     exit 1;
# fi

if [[ ! " ${ARGS[@]} " =~ " $1 " && ! $# = 0 ]]; then
    _usage " >>> ERROR : unknown argument '$1' ! ";
    exit 1;

elif [[ ! $# = 0 ]]; then

    eval set -- "$OPTS"

    while [[ ! -z "$1" || "$1" != "--" ]]  ; do 

        case "$1" in
            -h|--help) _usage;
                exit 0;;
            -q|--cluster) cluster="-g"
                shift;;
            -l|--launch) launch="-l"
                shift;;
            -v|--verbose) verbose="-v"
                shift;;
            -a|--amplicon_file) amplicon_file="-n $2 "
                shift 2;;
            -c|--core_genome_size) core_genome_size="$2"
                shift ;;
            -d|--barcode_size) barcode_size="$2";
                shift 2;;
            -e|--core_genome_number) core_genome_number="$2";
                shift 2;;           
            -f|--cgen_p_mutated) cgen_p_mutated="$2";
                shift 2;;
            -i|--cgen_max_mut_rate) cgen_max_mut_rate="$2";
                shift 2;;
            -g|--cgen_mut_over_indel_ratio) cgen_mut_over_indel_ratio="$2";
                shift 2;;
            -i|--read_size) read_size="$2";
                shift 2;;
            -j|--read_nb) read_nb="$2";
                shift 2;;
            -k|--simil_threshold) simil_threshold="$2";
                shift 2;;
            -m|--spacing_cseq) spacing_cseq="$2";
                shift 2;;
            -n|--threads) threads="$2";
                shift 2;;
            -o|--nb_max_pjobs) nb_max_pjobs="$2";
                shift 2;;
            -p|--nb_max_ram) nb_max_ram="$2";
                shift 2;;
            -r|--workdir) workdir="$2";
                shift 2;;   
            -z|--params) params="$2";
                shift 2;;
            -y|--email) email="-e $2";
                shift 2;;
            --) break;;
        esac
    done
# else; then
#     _usage " >>> ERROR : no argument given ! ";
#     exit 1;  
fi

# echo -n;
# echo "Updating python virtual environment path...";
# echo -n;
# sh ./setup.sh;

echo -n;
echo "Loading python virtual environment...";
echo -n;
source ./bin/activate;

echo "Python virtual environment succesfully activated!";
echo -n;

echo "Launch REDOSE workflow...";
echo -n;

cmd="python3 mk_workflow.py $amplicon_file -s1 $core_genome_size -s2 $barcode_size -s3 $core_genome_number -m1 $cgen_p_mutated -m2 $cgen_max_mut_rate -m3 $cgen_mut_over_indel_ratio -r1 $read_size -r2 $read_nb -th $simil_threshold -ss $spacing_cseq -t $threads -nj $nb_max_pjobs -nr $nb_max_ram $email -w $workdir $cluster $launch $verbose "

echo "Command submitted : $cmd"

$cmd

echo "REDOSE workflow successfully submitted !";
echo -n;

echo "Disable python virtual environment...";
echo -n;

deactivate;

echo "Python virtual environment succesfully disactivated!";
echo -n;

exit 0;
