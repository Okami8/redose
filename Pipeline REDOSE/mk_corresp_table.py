#!/usr/bin/env python3
# -*- coding: utf-8 -*-

###########
# IMPORTS #
###########

from Bio import pairwise2, AlignIO, SeqIO
from Bio.pairwise2 import format_alignment
import argparse, re, os, seqplot
from Bio.Align import MultipleSeqAlignment
from Bio.Alphabet import IUPAC, Gapped, generic_dna
from Bio.Seq import Seq
from Bio.SeqRecord import SeqRecord
from collections import Counter
import numpy as np 
import matplotlib.pyplot as plt
from matplotlib.text import TextPath
from matplotlib.patches import PathPatch
from matplotlib.font_manager import FontProperties
import random
import subprocess

########
# ARGS #
########

parser = argparse.ArgumentParser(description='Generate the output correspondence table')

parser.add_argument('-b', '--attB_file', required=True, help='attB input tabulated file')
parser.add_argument('-l', '--attL_file', required=True, help='attL input fasta file')
parser.add_argument('-r', '--attR_file', required=True, help='attR input fasta file')
parser.add_argument('-c', '--chunk_id', required=False, type=int, help='chunk file identifier (if input is splitted)')
parser.add_argument('-j', '--jobdir', type=str, required=False, default="./", help="Directory where workflow output files will be stored")
parser.add_argument('-v', '--verbose', required=False, action="store_true", help='Verbose output')

opt = parser.parse_args()

############
# FUNCTION #
############

def random_with_N_digits(n,hexadecimal=True):
    range_start = 10**(n-1)
    range_end = (10**n)-1
    if hexadecimal == True:
        return hex(random.randint(range_start, range_end))
    else:
        return random.randint(range_start, range_end)

def align_seq(file):
    os.system("./clustalo -i "+file+" -o "+file+"_align")
    return(file+"_align")

def removing_useless_files(file_name):
    if os.path.exists(file_name):
        os.remove(file_name)
    else:
        print("The file ",file_name," doesn't exist")

########
# MAIN #
########

def main():

    # On récupère les bases de chaque séquence pour chaque position donnée
    # align_pos = [ [seq[p] for seq in [s for s in hash.values()] ] for p in range(len([v for v in hash.values()][0]))]
    # On calcule les occurences de chaque base pour chaque position de l'alignement et on détermine le pourcentage global de similarité (le pourcentage de la base la plus fréquente)
    # counter_pos = [dict(Counter(bases)) for bases in align_pos]
    # Identify cluster of corresponding sequences among files (using barcode)

    print("\n")
    print("Build correspondence table...")
    print("\n")

    attB_file_core = open(opt.attB_file, "r")
    # attL_file_core = open(opt.attL_file, "r")
    # attR_file_core = open(opt.attR_file, "r")

    # attB_file_core.readline()
    # attL_file_core.readline()
    # attR_file_core.readline()

    # attL_sequences = [l.replace("\n",'').split('\t') for l in attL_file_core.readlines()]
    # attR_sequences = [r.replace("\n",'').split('\t') for r in attR_file_core.readlines()]
   
    output_file_path = opt.jobdir+"/"+"correspondence_table.csv"

    if opt.chunk_id:
        output_file_path = opt.jobdir+"/"+"correspondence_table_"+str(opt.chunk_id)+".csv"

    file_out = open(output_file_path, "w")

    #header = "attB_core\tattB_barcode1\tattB_barcode2\tattB_occ\tattL_core\tattL_barcode1\tattL_occ\tattR_core\tattR_barcode2\tattR_occ\tattP_rebuilt"
    #file_out.write(header)

    if opt.verbose == True:

        print(header)

    attB_all_sequences = []
    attP_all_sequences = []

    for b in attB_file_core.readlines():

        tmp_attB_barcode1, tmp_attB_core, tmp_attB_barcode2, tmp_attB_occ = b.replace("\n",'').split('\t')
       
        # Write temp sequence to search
        tmp_query_id = random_with_N_digits(8)
        tmp_attL_query_path = opt.jobdir+"/tmp_attL_query_"+tmp_query_id+"_coreseq.fa"
        tmp_attR_query_path = opt.jobdir+"/tmp_attR_query_"+tmp_query_id+"_coreseq.fa"

        # Write barcode1 and barcode2 blastn temp query files

        with open(tmp_attL_query_path,'w') as attLtmpquery:

            attLtmpquery.write(
            '''>attL_barcode_query
'''+re.sub("-","",tmp_attB_barcode1))

        with open(tmp_attR_query_path,'w') as attRtmpquery:

            attRtmpquery.write(
            '''>attR_barcode_query
'''+re.sub("-","",tmp_attB_barcode2))

        # Search corresponding attL using barcode1 / attR using barcode2

        search_attL = subprocess.check_output(
            "./blastn -query "+tmp_attL_query_path+" -subject "+opt.attL_file+" -word_size 5 -perc_identity 90 -qcov_hsp_perc 90 -outfmt=6",
            shell=True
        ).decode("utf-8")

        search_attR = subprocess.check_output(
            "./blastn -query "+tmp_attR_query_path+" -subject "+opt.attR_file+" -word_size 5 -perc_identity 90 -qcov_hsp_perc 90 -outfmt=6",
            shell=True
        ).decode("utf-8")

        os.system("rm "+tmp_attL_query_path)
        os.system("rm "+tmp_attR_query_path)
        
        if len(search_attL) > 0 and len(search_attR) > 0 and len(re.sub("-","",tmp_attB_barcode1)) >= len(tmp_attB_barcode1)/2 and len(re.sub("-","",tmp_attB_barcode2)) >= len(tmp_attB_barcode2)/2:
            # Search corresponding attL using barcode1
            attLseqid, tmp_attL_barcode1, tmp_attL_core, tmp_attL_occ = search_attL.split("\n")[0].split("\t")[1].split("|")
            # Search corresponding attR using barcode2
            attRseqid, tmp_attR_barcode2, tmp_attR_core, tmp_attR_occ = search_attR.split("\n")[0].split("\t")[1].split("|")

            # # Search corresponding attL using barcode1
            # tmp_attL_barcode1, tmp_attL_core, tmp_attL_occ = attL_sequences[np.argmax([pairwise2.align.localxs(bl, tmp_attB_barcode1, -2, -1, score_only = True) for bl in [s[0] for s in attL_sequences] ])]

            # # Search corresponding attR using barcode2
            # tmp_attR_core, tmp_attR_barcode2, tmp_attR_occ = attR_sequences[np.argmax([pairwise2.align.localxs(br, tmp_attB_barcode2, -2, -1, score_only = True) for br in [s[1] for s in attR_sequences] ])]

            # delete temp file

            # Align sequences

            #print("Realign identified core sequences...")

            tmp_attB_fa = re.sub('-','',tmp_attB_core)
            tmp_attL_fa = re.sub('-','',tmp_attL_core)
            tmp_attR_fa = re.sub('-','',tmp_attR_core)

            tmpseq_path = opt.jobdir+"/tmp_"+random_with_N_digits(8)+"_coreseq.fa"

            if len(tmp_attB_fa) > 0 and len(tmp_attL_fa) > 0 and len(tmp_attR_fa) > 0:

                with open(tmpseq_path, 'w') as tmpseq :

        #             print('''>attB_core
        # '''+tmp_attB_fa+'''
        # >attL_core
        # '''+tmp_attL_fa+'''
        # >attR_core
        # '''+tmp_attR_fa)

                    tmpseq.write(
'''>attB_core
'''+tmp_attB_fa+'''
>attL_core
'''+tmp_attL_fa+'''
>attR_core
'''+tmp_attR_fa)

                # align temprory sequences

                align_seq(tmpseq_path)  

                tmp_attB_core, tmp_attL_core, tmp_attR_core = AlignIO.read(tmpseq_path+"_align", "fasta")        

                # delete temp file

                os.system("rm "+tmpseq_path)
                os.system("rm "+tmpseq_path+"_align")

                # Build attP

                ltmp_attP_core = []

                tmp_attB_core = str(tmp_attB_core.seq)
                tmp_attL_core = str(tmp_attL_core.seq)
                tmp_attR_core = str(tmp_attR_core.seq)

                ltmp_attB_core = list(tmp_attB_core)
                ltmp_attL_core = list(tmp_attL_core)
                ltmp_attR_core = list(tmp_attR_core)

                # print("ltmp_attB_core : ",len(ltmp_attB_core)," => ",ltmp_attB_core)
                # print("ltmp_attL_core : ",len(ltmp_attL_core)," => ",ltmp_attL_core)
                # print("ltmp_attR_core : ",len(ltmp_attR_core)," => ",ltmp_attR_core)

                for i in range(len(ltmp_attB_core)):      
            
                    # print("INDEX : ",i)

                    if len(np.unique([ltmp_attB_core[i],ltmp_attL_core[i],ltmp_attR_core[i]])) == 1:
                        ltmp_attP_core.append(ltmp_attB_core[i])
                    elif ltmp_attL_core[i] != ltmp_attB_core[i] and ltmp_attR_core[i] != ltmp_attB_core[i]:
                        if len(np.unique([ltmp_attL_core[i],ltmp_attR_core[i]])) == 1:
                            ltmp_attP_core.append(ltmp_attL_core[i])
                        else:
                            ltmp_attP_core.append("{"+ltmp_attL_core[i]+","+ltmp_attR_core[i]+"}")
                    elif ltmp_attL_core[i] != ltmp_attB_core[i]:
                        ltmp_attP_core.append(ltmp_attL_core[i])
                    elif ltmp_attR_core[i] != ltmp_attB_core[i]:
                        ltmp_attP_core.append(ltmp_attR_core[i])

                tmp_attP_core = ''.join(ltmp_attP_core)

                attB_all_sequences.append(tmp_attB_core) 
                attP_all_sequences.append(tmp_attP_core)

                row = [tmp_attB_core, tmp_attB_barcode1, tmp_attB_barcode2, tmp_attB_occ, tmp_attL_core, tmp_attL_barcode1, tmp_attL_occ,tmp_attR_core, tmp_attR_barcode2, tmp_attR_occ, tmp_attP_core]

                if opt.verbose == True:

                    print('\t'.join(row))
                
                file_out.write('\t'.join(row)+"\n")

    attB_file_core.close()
    # attL_file_core.close()
    # attR_file_core.close()

    file_out.close()

    # VERBOSE OPTION : OPTIONAL DISPLAY :

    if opt.verbose:
        print(opt) # command line parameters
       
if __name__ == '__main__':

    main()