#!/usr/bin/env python3

###########
# IMPORTS #
###########vi 

import argparse, re, os

########
# ARGS #
########

parser = argparse.ArgumentParser(description='Main')

parser.add_argument('-f', '--input_file', required=True, help='Input fastq file to split')
parser.add_argument('-s', '--chunk_size', default=10, required=False, type=int, help='Maximum number of sequences per chunk')
parser.add_argument('-d', '--delimiter', default='\n@', required=False, type=str, help='Delimiter used to split sequences')
parser.add_argument('-e', '--extension', default='.fq', required=False, type=str, help='File extension')       
parser.add_argument('-n', '--nb_chunk_files', required=False, type=int, help='Number of chunked output files')
parser.add_argument('-v', '--verbose', required=False, action="store_true", help='Verbose output')

opt = parser.parse_args()

############
# FUNCTION #
############

# Split a given given file in subfiles
def split_chunks(file, delimiter=opt.delimiter, buff_size=opt.chunk_size, nb_files=opt.nb_chunk_files):

   splitted_file = [ l for l in file if len(l)>0 ]

   if opt.nb_chunk_files:
        if len(splitted_file) % opt.nb_chunk_files > 0:
            opt.nb_chunk_files -= 1
        buff_size = int(round(len(splitted_file)/opt.nb_chunk_files,0)) #+ (1 if (len(splitted_file) % opt.nb_chunk_files)>0 else 0))

   output = []
   while len(splitted_file) > 1:
        tmp = [ splitted_file.pop(0) for i in range(min(buff_size, len(splitted_file)))]
        output += [tmp]
        if len(splitted_file) == 1:
            output[len(output)-1].append(splitted_file[0])
   while len(output) < nb_files:
       output.append([])
   return output

########
# MAIN #
########

def main():      

    print("Split file "+str(opt.input_file)+" in chunk files of "+str(opt.chunk_size)+" sequences for "+str(opt.nb_chunk_files)+" files...")

    file = open(opt.input_file,'r')

    infile = ""

    if opt.delimiter == "linebreak":    
       opt.delimiter = "\n"

    infile = file.read().split(opt.delimiter)

    chunkify = split_chunks(infile, delimiter=opt.delimiter)

    count = 1

    while len(chunkify)>0:
        
        current_chunk = chunkify.pop(0)

        tmp_outfile = open(re.sub(opt.extension,"_"+str(count)+opt.extension,opt.input_file),'w')

        if len(current_chunk) > 0 :

            file_chunk=""

            if opt.delimiter == '\n@':
                    file_chunk = '@'+'\n@'.join(current_chunk)
            else:
                    file_chunk = opt.delimiter.join(current_chunk)

            if opt.verbose == True:
                    print(count," ",re.sub(opt.extension,"_"+str(count)+opt.extension,opt.input_file))
                    print(file_chunk)

            tmp_outfile.write(file_chunk)

        tmp_outfile.close()

        count+=1

    file.close()
        
if __name__ == '__main__':
    main()