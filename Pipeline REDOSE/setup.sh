#!/usr/bin/env bash 

# NEWPATH=`pwd | sed 's,/,\\\/,g'` ;

# echo "Setting virtual venv path ..." ;

# echo "Updating bin/activate script..."

# sed -i 's/VIRTUAL_ENV=".\+"/VIRTUAL_ENV="'$NEWPATH'"/g' bin/activate;
# sed -i 's/VIRTUAL_ENV ".\+"/VIRTUAL_ENV "'$NEWPATH'"/g' bin/activate;

# echo "Updating bin/activate.csh script..."

# sed -i 's/VIRTUAL_ENV=".\+"/VIRTUAL_ENV="'$NEWPATH'"/g' bin/activate.csh;
# sed -i 's/VIRTUAL_ENV ".\+"/VIRTUAL_ENV "'$NEWPATH'"/g' bin/activate.csh;

# echo "Updating bin/activate.fish script..."

# sed -i 's/VIRTUAL_ENV=".\+"/VIRTUAL_ENV="'$NEWPATH'"/g' bin/activate.fish;
# sed -i 's/VIRTUAL_ENV ".\+"/VIRTUAL_ENV "'$NEWPATH'"/g' bin/activate.fish;

# echo "Updating other bin files..."

# sed -i 's/#!.\+\/bin\/python/#!'$NEWPATH'\/bin\/python/g' bin/*

echo "Deleting past virtual environment..."

if [ -d bin ]; then
   echo " - Deleting old 'bin' folder"
   rm -rf bin
fi

if [ -d lib ]; then
   echo " - Deleting old 'lib' folder"
   rm -rf lib
fi

if [ -d lib64 ]; then
   echo " - Deleting old 'lib64' folder"
   rm -rf lib64
fi

if [ -d share ]; then
   echo " - Deleting old 'share' folder"
   rm -rf share
fi

if [ -d include ]; then
   echo " - Deleting old 'include' folder"
   rm -rf include
fi

if [ -d .snakemake ]; then
   echo " - Deleting old '.snakemake' folder"
   rm -rf .snakemake
fi

if [ -d __pycache__ ]; then
   echo " - Deleting old '__pycache__' folder"
   rm -rf __pycache__
fi

echo "Installing python3 virtual environment..."

PYTHON3="python3"

if  [ ! -x "$(command -v 'python3 - V')" ]; then

   PYTHON3='/tools/python/3.6.3/bin/python3'

fi

$PYTHON3 -m venv ./

source bin/activate

pip3 install -r requirements.txt

deactivate
