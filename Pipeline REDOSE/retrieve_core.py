#!/usr/bin/env python3

###########
# IMPORTS #
###########

from Bio import pairwise2, AlignIO, SeqIO
from Bio.pairwise2 import format_alignment
import argparse, re, os, seqplot
from Bio.Align import MultipleSeqAlignment
from Bio.Alphabet import IUPAC, Gapped, generic_dna
from Bio.Seq import Seq
from Bio.SeqRecord import SeqRecord
from collections import Counter
import numpy as np 
import matplotlib.pyplot as plt
from matplotlib.text import TextPath
from matplotlib.patches import PathPatch
from matplotlib.font_manager import FontProperties
import random

########
# ARGS #
########

parser = argparse.ArgumentParser(description='Identify barcodes and core sequences from attL, attR and attB read sequences')

parser.add_argument('-s1', '--core_sequence_size',type=int, default=14, required=False, help='generated core genome sequences size')
parser.add_argument('-s2', '--barcode_size', type=int, default=10, required=False, help='generated barcode sequences size')
parser.add_argument('-b', '--attB_file', required=True, help='attB input file')
parser.add_argument('-l', '--attL_file', required=True, help='attL input file')
parser.add_argument('-r', '--attR_file', required=True, help='attR input file')
parser.add_argument('-p', '--simil_plot', default='simil_plot.png', required=False, help='output PNG plot file displaying sequence similarity along alignment')
parser.add_argument('-d', '--display_plot', required=False, action="store_true", help="Display plots (T/F)")
parser.add_argument('-t', '--simil_threshold',type=int, default=45, required=False, help="Similarity threshold used to identify barcodes and core sequences")
parser.add_argument('-s', '--spacing_cseq',type=int, default=10, required=False, help="Minimal gap between two core sequences (used to split selected positions among corresponding sequences)")
parser.add_argument('-j', '--jobdir', type=str, required=False, default="./", help="Directory where workflow output files will be stored")
parser.add_argument('-v', '--verbose', required=False, action="store_true", help='Verbose output')

opt = parser.parse_args()

############
# FUNCTION #
############

def align_seq(file):
    os.system("./clustalo -i "+file+" -o "+file+"_align")
    return(file+"_align")

def similarity_plot(v_simil, v_simil2, v_simil3, seq_file, seq_file2, seq_file3, display):
    fig_size = plt.rcParams["figure.figsize"]  
    fig_size[0] = 12  
    fig_size[1] = 8  
    plt.rcParams["figure.figsize"] = fig_size  

    offset = 1 # pnp.sum(c.values())s de mesure

    nt = np.arange(0, len(v_simil), offset)
    nt2 = np.arange(0, len(v_simil2), offset)
    nt3 = np.arange(0, len(v_simil3), offset)

    simil = np.array(v_simil)
    simil2 = np.array(v_simil2)
    simil3 = np.array(v_simil3)

    fig, axes = plt.subplots(3,1)

    def axes_plot(nb,nucleotide,similarity,v_similarity,sequence_file):
        axes[nb].plot(nucleotide, similarity)
        axes[nb].set_xlim(0,len(v_similarity))
        axes[nb].set_xlabel('Position (nt)')
        axes[nb].set_ylabel('Similarity (%)')
        axes[nb].set_title(re.sub(r'\.fq\.fa$','',re.sub(r'^.*\/','',sequence_file)))
        axes[nb].grid(True)

    axes_plot(0,nt,simil,v_simil,seq_file)
    axes_plot(1,nt2,simil2,v_simil2,seq_file2)
    axes_plot(2,nt3,simil3,v_simil3,seq_file3)
    
    fig.tight_layout()
    fig.savefig(opt.jobdir+"/"+opt.simil_plot)

    if opt.display_plot==True:
        plt.show()

def convert_to_fasta(file):
    os.system(" sed -i 's/@/>/g' "+file)

def seq_similarity(alignment, file_path):
    h = {al.id:list(str(al.seq)) for al in alignment}
    # On récupère les bases de chaque séquence pour chaque position donnée
    align_pos = [ [seq[p] for seq in [s for s in h.values()] ] for p in range(len([v for v in h.values()][0]))]
    # On calcule les occurences de chaque base pour chaque position de l'alignement et on détermine le pourcentage global de similarité (le pourcentage de la base la plus fréquente)
    counter_pos = [dict(Counter(bases)) for bases in align_pos]

    # print(counter_pos)

    # for d in counter_pos:
    #     if '-' in d.keys():
    #         d['-']=0
    #     print(d)

    #sequence_similarity = [c.get(max(c,key=c.get))/len(alignment)*100 for c in counter_pos if list(c.keys())[0] != "-"]
    # print(alignment)

    # print(counter_pos)

    sequence_similarity = [c.get(max(c,key=c.get))/(sum(c.values()))*100 for c in counter_pos]

    # print(sequence_similarity)
    # print(sequence_similarity)
    
    if opt.verbose == True:
        print(" => "+file_path+" : ")
        print(sequence_similarity)
        print("\n")

    return sequence_similarity

def removing_useless_files(file_name):
    if os.path.exists(file_name):
        os.remove(file_name)
    else:
        print("The file ",file_name," doesn't exist")

########
# MAIN #
########

def main():

    print("Align read corresponding fasta sequences (clustalo) ...")
    print("\n")

    # Convert fastq sequence to fasta:

    convert_to_fasta(opt.attB_file)
    convert_to_fasta(opt.attL_file)
    convert_to_fasta(opt.attR_file)

    align_attB = align_seq(opt.attB_file)
    align_attL = align_seq(opt.attL_file)
    align_attR = align_seq(opt.attR_file)
    
    h_align = {
        "attB" : AlignIO.read(align_attB, "fasta"),
        "attL" : AlignIO.read(align_attL, "fasta"),
        "attR" : AlignIO.read(align_attR, "fasta")
    }

    print("\n")
    print("Compute positions sequence conservation...")
    print("\n")

    h_simil = {
        "attB" : seq_similarity(h_align["attB"], align_attB),
        "attL" : seq_similarity(h_align["attL"], align_attL),
        "attR" : seq_similarity(h_align["attR"], align_attR)
    }
    
    print("\n")
    print("Compute sequence positions similarity plot...")
    print("\n")

    similarity_plot(h_simil['attB'], h_simil['attL'], h_simil['attR'], opt.attB_file, opt.attL_file, opt.attR_file,opt.display_plot)

    # Identify core sequences positions (on alignment)

    h_cseq = {
        "attB" : {
            "bcode1"  : [],
            "core" : [],
            "bcode2" : []
        },
        "attL" : {
            "bcode1" : [],
            "core" : []
        },
        "attR" : {
            "core" : [],
            "bcode2" : []
        }
    }

    for k in h_simil.keys():
        # print("=> ",k)
        positions = [i for i, x in enumerate(h_simil[k]) if x <= opt.simil_threshold]
        # print(k)
        # print(h_simil[k])
        # print(positions)
        seqids = list(h_cseq[k].keys())
        # print(seqids)
        current_seq = seqids.pop(0)
        # print(current_seq)
        h_cseq[k][current_seq].append(positions[0])
        for i in range(1, len(positions)):      
            # print(positions[i]," ",positions[i-1])
            if len(seqids) >= 0:
                if positions[i] - positions[i-1] > opt.spacing_cseq:
                    # print(seqids)

                    # TODO : remove blocks if length < targeted sequencesm

                    if len(h_cseq[k][current_seq]) < max(opt.spacing_cseq,min(opt.barcode_size,opt.core_sequence_size)-opt.spacing_cseq):
                        h_cseq[k][current_seq] = []
                    else:
                        current_seq = seqids.pop(0)  
                    # print(current_seq)        
                h_cseq[k][current_seq].append(positions[i])
            # print(h_cseq[k][current_seq])
        # print("STOP")
        # print("BEF :",h_cseq[k][current_seq])
        p_min = min(h_cseq[k][current_seq])
        p_max = max(h_cseq[k][current_seq])
        h_cseq[k][current_seq] = list(range(p_min,p_max+1))
        # print("AFT :",h_cseq[k][current_seq])

    # print(h_cseq[k])

    # Retrieve corresponding sequences from alignement

    print("\n")
    print("Extract sequences of interest from alignments...")
    print("\n")

    hash_buffer = {}

    for s in h_cseq.keys():
        hash_buffer[s] = {}
        tmp_hash = {}

        # Retrieve positions corresponding to core sequences

        for f in h_cseq[s].keys():
            tmp_hash[f] = {al.id:[list(str(al.seq))[i] for i in h_cseq[s][f]] for al in h_align[s]}
            # print(f)
            # print(tmp_hash[f])

        # Corresponding sequences are joined and written into a csv file

        for r in tmp_hash[list(tmp_hash.keys())[0]].keys():
            ck = "\t".join([''.join(feature[r]) for feature in [tmp_hash[k] for k in tmp_hash.keys()]])
            if hash_buffer[s].get(ck) != None:
                hash_buffer[s][ck] += 1
            else:
                hash_buffer[s][ck] = 1

    for s in hash_buffer.keys():

        if s == "attB":

            file_core = open(opt.jobdir+"/"+s+"_coreseq_table.txt", "w")
            #file_core.write("\t".join(list(h_cseq[s].keys())+["Occ"]))
            for r in hash_buffer[s].keys():
                file_core.write("\t".join([r,str(hash_buffer[s][r])])+"\n")   
            file_core.close()

        else:

            file_core = open(opt.jobdir+"/"+s+"_coreseq.fa", "w")
            c=1
            for r in hash_buffer[s].keys():
                
                barcode = ""
                coreseq = ""
                occ = str(hash_buffer[s][r])

                if s == "attL":
                    barcode, coreseq = r.split("\t") 
                elif s == "attR":
                    coreseq, barcode = r.split("\t")
                
                header = "|".join([">seq"+str(c),barcode,coreseq,occ])

                file_core.write(header+"\n"+re.sub("-","",barcode)+"\n")

                c+=1

            file_core.close()

    # VERBOSE OPTION : OPTIONAL DISPLAY :

    if opt.verbose:
        print(opt) # command line parameters
       
if __name__ == '__main__':

    main()
