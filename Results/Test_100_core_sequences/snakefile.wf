IDS = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20]

# 10. Final output rule, correspond to analysis output file

rule all:
    input:
        "./REDOSE_job_1566771591/correspondence_table.csv",
        "./REDOSE_job_1566771591/weblogo_attB_attP.png",
        "./REDOSE_job_1566771591/simil_plot.png"
# 9. Generate weblogo sequence plot

rule mk_weblogo:
    output:
        "./REDOSE_job_1566771591/weblogo_attB_attP.png"
    input:
        "./REDOSE_job_1566771591/correspondence_table.csv"
    shell:
        "python3 mk_weblogo_plot.py -c {input} -w weblogo_attB_attP.png -j ./REDOSE_job_1566771591"

# 8. Merge attB table split files

rule merge_corresp_table:
    output:
        "./REDOSE_job_1566771591/correspondence_table.csv"
    input:
        expand("./REDOSE_job_1566771591/correspondence_table_{nbchunkB}.csv", nbchunkB=IDS)
    shell:
        '''
            echo "attB_core	attB_barcode1	attB_barcode2	attB_occ	attL_core	attL_barcode1	attL_occ	attR_core	attR_barcode2	attR_occ	attP_rebuilt
" > {output} ;
            cat ./REDOSE_job_1566771591/correspondence_table_*.csv >> {output} ;            
            sed -i '/^$/d' {output} ;
        '''
    
# 7. Reconstitute the AttP core sequences and identify corresponding sequences

rule rebuilt_consensus_core:
    output:
        temp("./REDOSE_job_1566771591/correspondence_table_{nbchunkB}.csv")
    input:
        attB="./REDOSE_job_1566771591/attB_coreseq_table_{nbchunkB}.txt",
        attL="./REDOSE_job_1566771591/attL_coreseq.fa",
        attR="./REDOSE_job_1566771591/attR_coreseq.fa",
    shell:
        '''
            if [ ! -s {input.attB} ]
            then
                touch {output} ;
            else
                python3 mk_corresp_table.py -b {input.attB} -l {input.attL} -r {input.attR} -c {wildcards.nbchunkB} -j ./REDOSE_job_1566771591 ;
            fi
        '''

# 6. AttB core sequence table into chunks

rule split_attB_table:
    output:
        temp("./REDOSE_job_1566771591/attB_coreseq_table_{nbchunkB}.txt")
    input:
        "./REDOSE_job_1566771591/attB_coreseq_table.txt"
    shell:
        '''
            python3 chunk.py -f ./REDOSE_job_1566771591/attB_coreseq_table.txt -n 20 -d "linebreak" -e ".txt"
        '''    

# 5. Reconstitute the AttB, AttL, AttR core sequences

rule identify_core:
    output:
        attB=temp("./REDOSE_job_1566771591/attB_coreseq_table.txt"),
        attL=temp("./REDOSE_job_1566771591/attL_coreseq.fa"),
        attR=temp("./REDOSE_job_1566771591/attR_coreseq.fa"),
        conservation_plot="./REDOSE_job_1566771591/simil_plot.png",
        attB_align=temp("./REDOSE_job_1566771591/attB.fq.fa_align"),
        attR_align=temp("./REDOSE_job_1566771591/attL.fq.fa_align"),
        attL_align=temp("./REDOSE_job_1566771591/attR.fq.fa_align")
    input:
        attB="./REDOSE_job_1566771591/attB.fq.fa",
        attL="./REDOSE_job_1566771591/attL.fq.fa",
        attR="./REDOSE_job_1566771591/attR.fq.fa"     
    shell:
        "python3 retrieve_core.py -s1 14 -s2 10 -b {input.attB} -l {input.attL} -r {input.attR} -p simil_plot.png -t 70 -s 10 -j ./REDOSE_job_1566771591"

# 4. Merge chunks into corresponding sequence type file

rule merge_files:
    output:
        attB="./REDOSE_job_1566771591/attB.fq.fa",
        attL="./REDOSE_job_1566771591/attL.fq.fa",
        attR="./REDOSE_job_1566771591/attR.fq.fa"
    input:
        attB = expand("./REDOSE_job_1566771591/attB_{nbchunkA}.fq.fa", nbchunkA=IDS),
        attL = expand("./REDOSE_job_1566771591/attL_{nbchunkA}.fq.fa", nbchunkA=IDS),
        attR = expand("./REDOSE_job_1566771591/attR_{nbchunkA}.fq.fa", nbchunkA=IDS)
    shell:
        '''
        cat ./REDOSE_job_1566771591/attB_*.fq.fa > {output.attB};
        cat ./REDOSE_job_1566771591/attL_*.fq.fa > {output.attL};
        cat ./REDOSE_job_1566771591/attR_*.fq.fa > {output.attR};
        ''' 

# 3. Identify sequences corresponding respectively to AttB, AttL and AttR

rule fastq_inspector:
    output:
        attB=temp("./REDOSE_job_1566771591/attB_{nbchunkA}.fq.fa"),
        attL=temp("./REDOSE_job_1566771591/attL_{nbchunkA}.fq.fa"),
        attR=temp("./REDOSE_job_1566771591/attR_{nbchunkA}.fq.fa")
    input:
        "./REDOSE_job_1566771591/amplicon_pair_{nbchunkA}.fq"
    shell:
        '''
            if [ ! -s {input} ]
            then
                touch {output.attB} ;
                touch {output.attL} ;
                touch {output.attR} ;
            else
                python3 fastq_split_MULTITHREAD.py -fq {input} -c {wildcards.nbchunkA} -t 6 -j ./REDOSE_job_1566771591 ;
            fi
        '''

# 2. Split amplicon fastq file in chunks

rule split_amplicon:
    output:
        temp("./REDOSE_job_1566771591/amplicon_pair_{nbchunkA}.fq")
    input:
        "./REDOSE_job_1566771591/amplicon_pair.fq"
    shell:    
        "python3 chunk.py -f {input} -n 20"


# 1. Create test illumina dataset

rule create_dataset:
    output:
        "./REDOSE_job_1566771591/amplicon_pair.fq",
        temp("./REDOSE_job_1566771591/attB_cgen.txt"),
        temp("./REDOSE_job_1566771591/attR_cgen.txt"),
        temp("./REDOSE_job_1566771591/attL_cgen.txt"),
        temp("./REDOSE_job_1566771591/bcode.txt"),
        temp("./REDOSE_job_1566771591/attB.fa"),
        temp("./REDOSE_job_1566771591/attL.fa"),
        temp("./REDOSE_job_1566771591/attR.fa"),
        temp("./REDOSE_job_1566771591/amplicon_pair_dat.sam"),
        temp("./REDOSE_job_1566771591/amplicon_pair_dat1.fq"),
        temp("./REDOSE_job_1566771591/amplicon_pair_dat2.fq"),
        temp("./REDOSE_job_1566771591/amp_reference.fa")
    input:
        "./REDOSE_job_1566771591/"
    shell:
        '''
        python3 dataset_creator.py -n amplicon_pair -s1 14 -s2 10 -s3 100 -m1 0.5 -m2 0.1 -m3 4.0 -r1 158 -r2 1 -j ./REDOSE_job_1566771591
        '''
